package cz.cvut.fel.webserver;

import javax.annotation.Nonnull;
import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.Map;

/**
 * Copyright 2015 IEAP CTU
 * Author: Jakub Begera (jakub.begera@utef.cvut.cz)
 */
public class CacheImpl<K, V> implements Cache<K, V> {
    private Map<K, SoftReference<V>> cache = new HashMap<K, SoftReference<V>>();

    public V get(K key) {
        SoftReference<V> softReference = cache.get(key);
        if (softReference == null) {
            return null;
        }
        V v = softReference.get();
        if (v == null) {
            cache.remove(key);
        }
        return v;
    }

    public void put(@Nonnull K key, @Nonnull V value) {
        cache.put(key, new SoftReference<V>(value));
    }

    public int getSize() {
        return cache.size();
    }

    public Map<K, SoftReference<V>> getHash() {
        return cache;
    }
}
