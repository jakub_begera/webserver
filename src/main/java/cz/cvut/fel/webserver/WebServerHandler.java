package cz.cvut.fel.webserver;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.*;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.List;

import static io.netty.handler.codec.http.HttpHeaders.Names.AUTHORIZATION;
import static io.netty.handler.codec.http.HttpResponseStatus.FORBIDDEN;
import static io.netty.handler.codec.http.HttpResponseStatus.NOT_FOUND;

/**
 * Copyright 2015 IEAP CTU
 * Author: Jakub Begera (jakub.begera@utef.cvut.cz)
 */
@ChannelHandler.Sharable
public class WebServerHandler extends SimpleChannelInboundHandler<FullHttpRequest> {
    private Cache<String, FullHttpResponse> cache;

    public WebServerHandler(Cache<String, FullHttpResponse> cache) {
        this.cache = cache;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext context, FullHttpRequest request) throws Exception {
        // check request
        if (request.getDecoderResult().isFailure()) {
            writeResponse(context, ResponseBuilder.buildErrorResponse(HttpResponseStatus.BAD_REQUEST));
            return;
        }

        // check method
        if (request.getMethod() != HttpMethod.GET) {
            writeResponse(context, ResponseBuilder.buildErrorResponse(HttpResponseStatus.METHOD_NOT_ALLOWED));
            return;
        }

        // path and file dir access
        String path;
        try {
            path = getPath(request.getUri());
            if (path == null) {
                writeResponse(context, ResponseBuilder.buildErrorResponse(HttpResponseStatus.FORBIDDEN));
                return;
            }
        } catch (RuntimeException ex) {
            switch (ex.getMessage()) {
                case "404": {
                    writeResponse(context, ResponseBuilder.buildErrorResponse(HttpResponseStatus.NOT_FOUND));
                    break;
                }
                case "403": {
                    writeResponse(context, ResponseBuilder.buildErrorResponse(HttpResponseStatus.FORBIDDEN));
                    break;
                }
            }
            return;
        }


        //authorization
        if (isDirAuthorizationRequired(path)) {
            if (!isUserAuthorized(request, path)) {
                writeResponse(context, ResponseBuilder.buildAuthResponse());
                return;
            }
        }

        //cache
        FullHttpResponse response;
        if ((response = cache.get(path)) == null) { // cache mis
            response = ResponseBuilder.buildResponseNormal(path);
            cache.put(path, response);
            WebServer.incrementCacheMis();
        } else {
            WebServer.incrementCacheHit();
        }

        WebServer.printCacheHitMis();
        writeResponse(context, response.copy());
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext context, Throwable cause) {
        System.err.println("Caught exception:");
        cause.printStackTrace();
        if (context.channel().isActive()) {
            writeResponse(context, ResponseBuilder.buildErrorResponse(HttpResponseStatus.INTERNAL_SERVER_ERROR));
        }
    }

    private void writeResponse(ChannelHandlerContext channelHandlerContext, FullHttpResponse response) {
        channelHandlerContext.write(response);
        channelHandlerContext.writeAndFlush(LastHttpContent.EMPTY_LAST_CONTENT);

    }


    //**********************************************************************************************************************
//  PUBLIC STATIC METHODS
//**********************************************************************************************************************
    public static String getPath(String uri) {
        try {
            uri = URLDecoder.decode(uri, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
        if (uri.isEmpty() || !uri.startsWith("/")) {
            return null;
        }

        String retPath = null;
        try {
            File file = new File(WebServer.SERVER_FILES_STORAGE_DIR + uri);
            if (!file.exists() || !file.isFile() || !file.canRead()) {
                throw new RuntimeException(String.valueOf(NOT_FOUND.code()));
            }
            retPath = file.getCanonicalPath();
            if (!retPath.startsWith(new File(WebServer.SERVER_FILES_STORAGE_DIR).getCanonicalPath())) {
                throw new RuntimeException(String.valueOf(FORBIDDEN.code()));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return retPath;
    }


    public static boolean isDirAuthorizationRequired(String path) {
        if (path == null) return false;
        File htaccess = new File(new File(path).getParent() + "/.htaccess");
        if (htaccess.exists() && htaccess.canRead()) {
            return true;
        }
        return false;
    }

    public static boolean isUserAuthorized(FullHttpRequest request, String path) {
        String requestHeaders;
        if ((requestHeaders = request.headers().get(AUTHORIZATION)) == null) return false;

        String userNameAndPass = requestHeaders.split(" ")[1], userName, pass, passHas;
        userNameAndPass = new String(Base64.getDecoder().decode(userNameAndPass), Charset.forName("UTF-8"));
        String[] pom = userNameAndPass.split(":");
        userName = pom[0];
        pass = pom[1];
        passHas = hashUsernameAndPass(pass);
        String userNameAndPassHas = userName + ":" + passHas;

        File htaccessFile = new File(new File(path).getParent() + "/.htaccess");
        List<String> htaccessFileLines;
        try {
            htaccessFileLines = Files.readAllLines(htaccessFile.toPath());
        } catch (IOException e) {
            return true;
        }
        if (htaccessFileLines.contains(userNameAndPassHas)) {
            return true;
        } else {
            return false;
        }
    }

    public static String hashUsernameAndPass(String password) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(password.getBytes(Charset.forName("UTF-8")));
            byte[] bytes = md.digest();
            StringBuilder sb = new StringBuilder();
            for (Byte b : bytes) {
                sb.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }


}
