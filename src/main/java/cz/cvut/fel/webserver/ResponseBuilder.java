package cz.cvut.fel.webserver;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.util.CharsetUtil;

import javax.activation.MimetypesFileTypeMap;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

import static io.netty.handler.codec.http.HttpHeaders.Names.CONTENT_TYPE;
import static io.netty.handler.codec.http.HttpHeaders.Names.WWW_AUTHENTICATE;
import static io.netty.handler.codec.http.HttpResponseStatus.*;
import static io.netty.handler.codec.http.HttpVersion.HTTP_1_1;

/**
 * Copyright 2015 IEAP CTU
 * Author: Jakub Begera (jakub.begera@utef.cvut.cz)
 */
public class ResponseBuilder {

    public static FullHttpResponse buildErrorResponse(HttpResponseStatus responseStatus) {
        ByteBuf msg = Unpooled.copiedBuffer("Failure: " + responseStatus + "\r\n", CharsetUtil.UTF_8);
        FullHttpResponse out = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, responseStatus, msg);
        out.headers().set(CONTENT_TYPE, "text/plain; charset=UTF-8");
        byte[] content = String.format("Failure: %s", responseStatus.toString()).getBytes(Charset.forName("UTF-8"));
        out.headers().setContentLength(out, content.length);
        out.content().writeBytes(content);
        return out;
    }


    public static FullHttpResponse buildResponseNormal(String path) {
        File file = new File(path);
        if (!file.exists() || !file.canRead()) {
            return buildErrorResponse(NOT_FOUND);
        }
        if (!file.isFile()) {
            return buildErrorResponse(NOT_FOUND);
        }

        byte[] fileB;
        try {
            fileB = Files.readAllBytes(Paths.get(path));
        } catch (IOException e) {
            e.printStackTrace();
            return buildErrorResponse(INTERNAL_SERVER_ERROR);
        }

        MimetypesFileTypeMap mimeTypesMap = new MimetypesFileTypeMap();
        mimeTypesMap.addMimeTypes("text/css css");
        mimeTypesMap.addMimeTypes("image/jpeg jpg");

        FullHttpResponse response = new DefaultFullHttpResponse(HTTP_1_1, OK);
        response.headers().setContentLength(response, fileB.length);
        response.headers().set(CONTENT_TYPE, mimeTypesMap.getContentType(file.getPath()));
        response.content().writeBytes(fileB);


        return response;
    }

    public static FullHttpResponse buildAuthResponse() {
        FullHttpResponse response = new DefaultFullHttpResponse(HTTP_1_1, UNAUTHORIZED);
        response.headers().set(WWW_AUTHENTICATE, "Basic realm=\"Secure Area\"");
        String s = "<HTML>\n" +
                " <HEAD>\n" +
                "   <TITLE>Error</TITLE>\n" +
                "   <META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; charset=ISO-8859-1\">\n" +
                " </HEAD>\n" +
                "   <BODY><H1>401 Unauthorised.</H1></BODY>\n" +
                " </HTML>";
        byte[] bytes = s.getBytes(Charset.forName("UTF-8"));
        response.headers().setContentLength(response, bytes.length);
        response.content().writeBytes(bytes);
        return response;
    }
}
