package cz.cvut.fel.webserver;

import javax.annotation.Nonnull;
import java.lang.ref.SoftReference;
import java.util.Map;

/**
 * Copyright 2015 IEAP CTU
 * Author: Jakub Begera (jakub.begera@utef.cvut.cz)
 */
public interface Cache<K, V> {

    V get(K key);

    void put(@Nonnull K key, @Nonnull V value);

    int getSize();

    Map<K, SoftReference<V>> getHash();


}