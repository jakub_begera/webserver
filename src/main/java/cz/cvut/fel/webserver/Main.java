package cz.cvut.fel.webserver;

/**
 * Copyright 2015 IEAP CTU
 * Author: Jakub Begera (jakub.begera@utef.cvut.cz)
 */
public class Main {

    public static void main(String[] args) throws Exception {
        WebServer webServer = new WebServer();
        webServer.run();
    }
}
