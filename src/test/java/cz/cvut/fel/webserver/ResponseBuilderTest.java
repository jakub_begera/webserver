package cz.cvut.fel.webserver;

import cz.cvut.fel.webserver.ResponseBuilder;
import cz.cvut.fel.webserver.WebServer;
import cz.cvut.fel.webserver.WebServerHandler;
import io.netty.handler.codec.http.*;
import junit.framework.TestCase;

import java.nio.charset.Charset;
import java.nio.file.Paths;

import static io.netty.handler.codec.http.HttpHeaders.Names.*;

/**
 * Copyright 2015 IEAP CTU
 * Author: Jakub Begera (jakub.begera@utef.cvut.cz)
 */
public class ResponseBuilderTest extends TestCase {

    @org.junit.Test
    public void testBuildErrorRexponse() throws Exception {
        FullHttpResponse response = ResponseBuilder.buildErrorResponse(HttpResponseStatus.FORBIDDEN);

        assertEquals(HttpVersion.HTTP_1_1, response.getProtocolVersion());
        assertEquals(HttpResponseStatus.FORBIDDEN, response.getStatus());
    }

    @org.junit.Test
    public void testGetPath() throws Exception {
        String path = WebServerHandler.getPath("/testpagesecured/index.html");

        assertEquals(Paths.get("").toAbsolutePath() + "/files/testpagesecured/index.html", path);
    }

    @org.junit.Test
    public void testBuildNormalRexponse() throws Exception {
        FullHttpResponse response = ResponseBuilder.buildResponseNormal(WebServer.SERVER_FILES_STORAGE_DIR + "/testpage/index.html");
        String content = "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "<title>Test Page</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "\n" +
                "<h1>This is a test page</h1>\n" +
                "\n" +
                "<img src=\"img/computer.jpg\" alt=\"Computer\">\n" +
                "\n" +
                "</body>\n" +
                "</html>";

        assertEquals(HttpVersion.HTTP_1_1, response.getProtocolVersion());
        assertEquals(HttpResponseStatus.OK, response.getStatus());
        assertEquals("text/html", response.headers().get(CONTENT_TYPE));
        assertEquals(content, new String(response.content().array(), Charset.forName("UTF-8")).trim());
    }

    @org.junit.Test
    public void testBuildAuthResponse() throws Exception {
        FullHttpResponse response = ResponseBuilder.buildAuthResponse();

        assertEquals(HttpVersion.HTTP_1_1, response.getProtocolVersion());
        assertEquals(HttpResponseStatus.UNAUTHORIZED, response.getStatus());
        assertEquals("Basic realm=\"Secure Area\"", response.headers().get(WWW_AUTHENTICATE));
    }

    @org.junit.Test
    public void testIsDirAuthorizationRequired() throws Exception {
        boolean secured = WebServerHandler.isDirAuthorizationRequired(
                WebServer.SERVER_FILES_STORAGE_DIR + "/testpagesecured/index.html");
        boolean noSecured = WebServerHandler.isDirAuthorizationRequired(
                WebServer.SERVER_FILES_STORAGE_DIR + "/testpage/index.html");

        assertTrue(secured);
        assertFalse(noSecured);
    }

    @org.junit.Test
    public void testIsUserAuthorized() throws Exception {
        FullHttpRequest request = new DefaultFullHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.GET, "/testpagesecured/index.html");
        boolean isUserAuthorized = WebServerHandler.isUserAuthorized(request,
                WebServer.SERVER_FILES_STORAGE_DIR + "/testpagesecured/index.html");
        assertFalse(isUserAuthorized);

        request.headers().add(AUTHORIZATION, "Basic YWRtaW46YWRtaW4=");
        isUserAuthorized = WebServerHandler.isUserAuthorized(request,
                WebServer.SERVER_FILES_STORAGE_DIR + "/testpagesecured/index.html");
        assertTrue(isUserAuthorized);

        request = new DefaultFullHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.GET, "/testpagesecured/index.html");
        request.headers().add(AUTHORIZATION, "Basic YWRdaW46YWRtaW4=");
        isUserAuthorized = WebServerHandler.isUserAuthorized(request,
                WebServer.SERVER_FILES_STORAGE_DIR + "/testpagesecured/index.html");
        assertFalse(isUserAuthorized);


    }


}