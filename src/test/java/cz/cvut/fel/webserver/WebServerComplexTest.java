package cz.cvut.fel.webserver;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.BeforeClass;

import static io.netty.handler.codec.http.HttpHeaders.Names.AUTHORIZATION;
import static io.netty.handler.codec.http.HttpResponseStatus.*;
import static org.junit.Assert.assertEquals;

/**
 * Copyright 2015 IEAP CTU
 * Author: Jakub Begera (jakub.begera@utef.cvut.cz)
 */
public class WebServerComplexTest {
    public static Thread serverThread = new Thread(new Runnable() {
        @Override
        public void run() {
            WebServer webServer = new WebServer();
            try {
                webServer.run();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    });


    @BeforeClass
    public static void runServer() throws Exception {
        serverThread.setDaemon(true);
        serverThread.start();

    }

    @org.junit.Test
    public void testGetIndex() throws Exception {
        HttpClient client = HttpClientBuilder.create().build();
        HttpUriRequest httpRequest = new HttpGet("http://localhost:8080/testpage/index.html");
        HttpResponse response = client.execute(httpRequest);
        HttpEntity entity = response.getEntity();
        String responseString = EntityUtils.toString(entity, "UTF-8");
        String content = "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "<title>Test Page</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "\n" +
                "<h1>This is a test page</h1>\n" +
                "\n" +
                "<img src=\"img/computer.jpg\" alt=\"Computer\">\n" +
                "\n" +
                "</body>\n" +
                "</html>";

        assertEquals("HTTP/1.1", response.getStatusLine().getProtocolVersion().toString());
        assertEquals(200, response.getStatusLine().getStatusCode());
        assertEquals("OK", response.getStatusLine().getReasonPhrase());
        assertEquals(content, responseString);
    }


    @org.junit.Test
    public void testGetIndexSecured() throws Exception {
        HttpClient client = HttpClientBuilder.create().build();
        HttpUriRequest httpRequest = new HttpGet("http://localhost:8080/testpagesecured/index.html");
        httpRequest.setHeader(AUTHORIZATION, "Basic YWRtaW46YWRtaW4=");
        HttpResponse response = client.execute(httpRequest);
        HttpEntity entity = response.getEntity();
        String responseString = EntityUtils.toString(entity, "UTF-8");
        String content = "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "<title>Secured test Page</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "\n" +
                "<h1>This is a <strong>secured</strong> test page</h1>\n" +
                "\n" +
                "<img src=\"img/computer.jpg\" alt=\"Computer\">\n" +
                "\n" +
                "</body>\n" +
                "</html>";

        assertEquals("HTTP/1.1", response.getStatusLine().getProtocolVersion().toString());
        assertEquals(200, response.getStatusLine().getStatusCode());
        assertEquals("OK", response.getStatusLine().getReasonPhrase());
        assertEquals(content, responseString);
    }

    @org.junit.Test
    public void testGetIndexSecuredAuthFailed() throws Exception {
        HttpClient client = HttpClientBuilder.create().build();
        HttpUriRequest httpRequest = new HttpGet("http://localhost:8080/testpagesecured/index.html");
        httpRequest.setHeader(AUTHORIZATION, "Basic YsRtaW46YWRtaW4=");
        HttpResponse response = client.execute(httpRequest);

        assertEquals("HTTP/1.1", response.getStatusLine().getProtocolVersion().toString());
        assertEquals(UNAUTHORIZED.code(), response.getStatusLine().getStatusCode());
    }

    @org.junit.Test
    public void testForbiden() throws Exception {
        HttpClient client = HttpClientBuilder.create().build();
        HttpUriRequest httpRequest = new HttpGet("http://localhost:8080/../pom.xml");
        httpRequest.setHeader(AUTHORIZATION, "Basic YsRtaW46YWRtaW4=");
        HttpResponse response = client.execute(httpRequest);

        assertEquals("HTTP/1.1", response.getStatusLine().getProtocolVersion().toString());
        assertEquals(FORBIDDEN.code(), response.getStatusLine().getStatusCode());
    }

    @org.junit.Test
    public void testNotFound() throws Exception {
        HttpClient client = HttpClientBuilder.create().build();
        HttpUriRequest httpRequest = new HttpGet("http://localhost:8080/testpage/index2.html");
        httpRequest.setHeader(AUTHORIZATION, "Basic YsRtaW46YWRtaW4=");
        HttpResponse response = client.execute(httpRequest);

        assertEquals("HTTP/1.1", response.getStatusLine().getProtocolVersion().toString());
        assertEquals(NOT_FOUND.code(), response.getStatusLine().getStatusCode());
    }



}